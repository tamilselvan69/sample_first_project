
import requests

# Zoho Books API endpoint
ZOHO_BOOKS_API_URL = "https://books.zoho.com/api/v3/"

# Your Zoho Books API credentials
CLIENT_ID = "1000.FBUDG9NQ4K0G682LKXYLIP5J0CokX"
CLIENT_SECRET = "89986a6a02cb222074eaf55e705669a874dc7cc"
REFRESH_TOKEN = "1000.6b70a5d33fd9f6a57369b5e3765fd.6deb8e4fffsefe24f5a1e0d68418f163708f8"

def get_access_token():
    token_url = "https://accounts.zoho.com/oauth/v2/token"
    payload = {
        "client_id": CLIENT_ID,
        "client_secret": CLIENT_SECRET,
        "refresh_token": REFRESH_TOKEN,
        "grant_type": "refresh_token"
    }
    response = requests.post(token_url, data=payload)
    access_token = response.json()["access_token"]
    return access_token

def create_invoice(access_token, data):
    url = ZOHO_BOOKS_API_URL + "invoices"
    headers = {
        "Authorization": f"Zoho-oauthtoken {access_token}",
        "Content-Type": "application/json"
    }
    response = requests.post(url, headers=headers, json=data)
    if response.status_code == 200:
        print("Invoice created successfully!")
    else:
        print("Failed to create invoice:", response.json())

def main():
    access_token = get_access_token()

    # Example data for creating an invoice (modify as needed)
    invoice_data = {
        "customer_id": "CUSTOMER_ID",
        "line_items": [
            {
                "item_id": "ITEM_ID",
                "rate": 100,
                "quantity": 2
            }
        ]
    }

    create_invoice(access_token, invoice_data)

if __name__ == "__main__":
    main()
